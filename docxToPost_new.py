# @Author: htyao
# @Date:   2020-05-16T22:22:23+02:00
# @Last modified by:   htyao
# @Last modified time: 2020-06-06T03:48:34+02:00


import sys
from datetime import date
from docx import Document
import re

vColors = ['0070C0', '2E74B5']
pColors = ['FF0000']
vocColors = ['7030A0']

op = re.compile("(.*)\((.*)")
cp = re.compile("(.*)\)(.*)")


def printParticlePage(article, vocList):
    """
    Write Particle practice file
    """
    t = ""
    for p in article:
        hasVoc = False
        voc = ""
        particle = ""
        t += "<p>"
        for r in p:
            if r[4]:
                continue
            elif r[1] and r[2]:
                # voc += r[0]
                particle += r[0]
                hasVoc = True
            elif r[1]:
                if particle and hasVoc:
                    voc += '<cloze data-hint="">{}</cloze>'.format(particle)
                    particle = ""
                elif particle:
                    t += '<cloze data-hint="">{}</cloze>'.format(particle)
                    particle = ""
                voc += r[0]
                hasVoc = True
            elif r[2]:
                if hasVoc:
                    t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                        "{}", "{}", voc)
                    voc = ""
                    hasVoc = False
                particle += r[0]
            else:
                if hasVoc:
                    if particle:
                        voc += '<cloze data-hint="">{}</cloze>'.format(
                            particle)
                        particle = ""
                    t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                        "{}", "{}", voc)
                    voc = ""
                    hasVoc = False
                    t += r[0]
                elif particle:
                    t += '<cloze data-hint="">{}</cloze>'.format(particle)
                    particle = ""
                    t += r[0]
                else:
                    t += r[0]
        t += "</p>\n"

    with open('_posts/{}-{}-Particle-연습.html'.format(DATE, SLG), 'w') as f:
        print("---", file=f)
        print("title: Particle 연습".format(TITLE), file=f)
        print("layout: cloze_page", file=f)
        print("categories: {}".format(CAT), file=f)
        print("tags: cloze", file=f)
        print("---", file=f)
        print(t.format(*vocList), file=f)


def printVerbPage(article, vocList):
    """
    Write Verb/Adj practice file
    """

    t = ""
    for p in article:
        hasVoc = False
        voc = ""
        verb = ""
        vHint = ""
        t += "<p>"
        for r in p:
            # print(r, verb)
            if r[4]:
                vHint += r[0]
            else:
                if vHint:
                    assert verb, "vHint assigned without verb: {}".format(
                        vHint)
                    if hasVoc:
                        voc += '<cloze data-hint="{}">{}</cloze>'.format(
                            vHint, verb)
                        verb = ""
                        vHint = ""
                        t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                            "{}", "{}", voc)
                        voc = ""
                        hasVoc = False
                    else:
                        t += '<cloze data-hint="{}">{}</cloze>'.format(
                            vHint, verb)
                        verb = ""
                        vHint = ""
                if r[1] and r[3]:
                    # voc += r[0]
                    verb += r[0]
                    hasVoc = True
                elif r[1]:
                    voc += r[0]
                    hasVoc = True
                elif r[3]:
                    if hasVoc:
                        t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                            "{}", "{}", voc)
                        voc = ""
                        hasVoc = False
                    verb += r[0]
                else:
                    if hasVoc:
                        t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                            "{}", "{}", voc)
                        voc = ""
                        hasVoc = False
                    t += r[0]
        t += "</p>\n"

    with open('_posts/{}-{}-Verb-Adjective-Conjugation-연습.html'.format(DATE, SLG), 'w') as f:
        print("---", file=f)
        print("title: Verb/Adjective Conjugation 연습", file=f)
        print("layout: cloze_page", file=f)
        print("categories: {}".format(CAT), file=f)
        print("tags: cloze", file=f)
        print("---", file=f)
        print(t.format(*vocList), file=f)


def printVocPage(article, vocList):
    """
    Write Original file (with inner voc)
    """
    t = ""
    vocCount = 0
    for p in article:
        voc = ""
        t += "<p>"
        for r in p:
            if r[4]:
                continue
            elif r[1]:
                voc += r[0]
            else:
                if voc:
                    t += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format(
                        "{}", "{}", voc)
                    voc = ""
                    t += r[0]
                    vocCount += 1
                else:
                    t += r[0]
        t += "</p>\n"

    assert vocCount == len(
        vocList) / 2, "Underlined: {}, Voc: {}\n{}".format(vocCount, len(vocList) / 2, underList)

    with open('_posts/{}-{}-vocabulary.html'.format(DATE, SLG), 'w') as f:
        print("---", file=f)
        print("title: {}".format(TITLE), file=f)
        print("layout: voc_page", file=f)
        print("categories: {}".format(CAT), file=f)
        print("tags: vocabulary", file=f)
        print("---", file=f)
        print(t.format(*vocList), file=f)


if __name__ == "__main__":
    fileName = sys.argv[1]
    doc = Document(fileName)

    DATE = date.today().isoformat()
    TITLE = doc.paragraphs[0].text
    SLG = TITLE.replace(" ", "-")
    CAT = TITLE.replace(" ", "")
    article = []
    originText = []
    vocText = []
    particleText = []
    verbText = []
    vocTmp = ""
    underNb = 0
    underList = []
    # Parse by paragraph
    for p in doc.paragraphs[1:]:
        tmp = []
        if "단어" in p.text:
            continue
        elif p.text:
            vHint = False
            v = ""
            for run in p.runs:
                # print("\'{}\'".format(run.text))
                # Check word's color
                # Red for particle
                # Blue for verb/adj
                # Purple for vocabulary section
                color = str(run.font.color.rgb)
                opm = op.match(run.text)
                cpm = cp.match(run.text)

                under = bool(run.underline)

                if color in pColors:
                    tmp.append((run.text, under, True, False, False))
                elif color in vColors:
                    if opm:
                        if not opm.group(1) == "":
                            tmp.append((opm.group(1), under, False, True, False))
                        if not opm.group(2) == "":
                            tmp.append((opm.group(2), False, False, False, True))
                        vHint = True
                    elif cpm:
                        if not cpm.group(1) == "":
                            tmp.append((cpm.group(1), False, False, False, True))
                        if not cpm.group(2) == "":
                            tmp.append((cpm.group(2), False, False, False, False))
                        vHint = False
                    elif vHint:
                        tmp.append((run.text, False, False, False, True))
                    else:
                        tmp.append((run.text, under, False, True, False))
                elif color in vocColors:
                    vocTmp += run.text
                    continue

                else:
                    tmp.append((run.text, under, False, False, False))

            article.append(tmp)
    vocList = []
    for t in vocTmp.split(","):
        l = t.split(":")
        assert len(l) == 2, "Separator missing. {}".format(t)
        vocList += l

    printVocPage(article, vocList)
    printParticlePage(article, vocList)
    printVerbPage(article, vocList)

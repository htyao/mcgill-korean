/**
 * @Author: htyao
 * @Date:   2020-05-16T22:22:23+02:00
 * @Last modified by:   htyao
 * @Last modified time: 2020-05-24T22:32:31+02:00
 */



String.prototype.format = function() {
  a = this;
  for (k in arguments) {
    a = a.replace("{" + k + "}", arguments[k])
  }
  return a
}

////////////////////////////////////
//                                //
//            To Top              //
//                                //
////////////////////////////////////
/*
function scrollBackTop () {
  var backTop = document.getElementById("back-top");
  if (window.scrollY > 100) {
    backTop.classList.remove("fade");
  } else {
    backTop.classList.add("fade");
  }
}
*/
////////////////////////////////////
//                                //
//              Voc               //
//                                //
////////////////////////////////////

function showInlineTrans() {
  var content = document.getElementById("article-content");
  content.classList.add("sparse-line");
  content.querySelectorAll("voc").forEach(function (voc) {
    var trans = document.createElement("span");
    trans.classList.add("trans");
    trans.innerHTML = voc.getAttribute("data-trans");
    voc.appendChild(trans);
  });
}

function hideInlineTrans() {
  var content = document.getElementById("article-content");
  content.classList.remove("sparse-line");
  document.querySelectorAll(".trans").forEach(function (trans) {
    trans.remove();
  });
}

function showVocBlock() {
  var vocBlock = document.getElementById("vocBlock");
  vocBlock.classList.remove("hide");
}

function hideVocBlock() {
  var vocBlock = document.getElementById("vocBlock");
  vocBlock.classList.add("hide");
}

function printVocInBlock () {
  var target = document.getElementById("vocs");
  var tmp = document.createElement("div");
  tmp.innerHTML = target.getAttribute("data-value");
  var s = "";
  tmp.querySelectorAll("voc").forEach(function (voc) {
    s += "<span>{0} {1}</span> ".format(voc.getAttribute("data-voc"), voc.getAttribute("data-trans"));
  });
  target.innerHTML = s;
}

function toggleVoc() {
  var btn = document.getElementById("vocBtn");
  // var modeBtn = document.getElementById("vocModeBtn");
  if (btn.innerHTML == "Show Vocabulary") {
    btn.innerHTML = "Hide Vocabulary";
    // showInline = false;
    // if (modeBtn) {
    //   modeBtn.classList.remove("hide");
    //   showInline = (modeBtn.innerHTML == "Seperate Mode");
    // }
    // if (showInline) {
    //   showInlineTrans();
    // } else {
      showVocBlock();
    // }
  } else {
    btn.innerHTML = "Show Vocabulary";
    // if (modeBtn) modeBtn.classList.add("hide");
    // hideInlineTrans();
    hideVocBlock();
  }
}

function changeVocMode() {
  var modeBtn = document.getElementById("vocModeBtn");
  var vocBlock = document.getElementById("vocBlock");
  if (modeBtn.innerHTML == "Seperate Mode") {
    modeBtn.innerHTML = "Inner Mode";
    vocBlock.classList.remove("hide");
    hideInlineTrans();
  } else {
    modeBtn.innerHTML = "Seperate Mode";
    vocBlock.classList.add("hide");
    showInlineTrans();
  }
}

////////////////////////////////////
//                                //
//              Cloze             //
//                                //
////////////////////////////////////

function replaceCloze() {
  document.querySelectorAll("cloze").forEach(function (cloze) {
    var hint = cloze.getAttribute("data-hint");
    // if (hint) hint = '('+hint+')';
    const ans = cloze.innerText;
    console.log(hint, ans);
    var span = document.createElement("span");
    span.classList.add("cloze");
    // var template = '<input class="cloze" type="text" size={0} data-answer={1} /><label>{2}</label><label class="fail hide">({3})</label>';
    var template = '<input class="cloze" onchange="clearStatus(this)" type="text" size="{0}" data-answer="{1}" placeholder="{2}">';
    span.innerHTML = template.format(ans.length+1, ans, hint);
    console.log(span)
    cloze.replaceWith(span);
  });
}

function checkAnswer() {
  document.querySelectorAll("span.cloze").forEach(function (span) {
    const list = span.children;
    var input = list[0];
    // var hintLabel = list[1];
    // var answerLabel = list[2];
    var trueAnswer = input.getAttribute("data-answer");
    var answer = input.value;
    if (trueAnswer == answer) {
      input.classList.remove("fail");
      input.classList.add("success");
      // answerLabel.classList.add('hide');
    } else {
      input.classList.remove("success");
      input.classList.add("fail");
      // hintLabel.classList.add("hide");
      // answerLabel.classList.remove('hide');
    };
  });
  // Close the nav list if applicable
  var checkbox =  document.getElementById("navbar-toggle-cbox");
  if (checkbox.checked) checkbox.checked = false;
}

function clearStatus(el) {
  el.classList.remove("fail");
  el.classList.remove("success");
}

function resetCloze() {
  document.querySelectorAll("span.cloze").forEach(function (span) {
    const list = span.children;
    var input = list[0];
    // var hintLabel = list[1];
    // var answerLabel = list[2];
    input.classList.remove("fail");
    input.classList.remove("success");
    input.value = "";
    // answerLabel.classList.add("hide");
    // hintLabel.classList.remove("hide");
  });
  var checkbox =  document.getElementById("navbar-toggle-cbox");
  if (checkbox.checked) checkbox.checked = false;
}

////////////////////////////////////
//                                //
//        Event Listeners         //
//                                //
////////////////////////////////////

function eventListeners() {
  //window.addEventListener('scroll', scrollBackTop);
  // document.getElementById("back-top").addEventListener('click', function () {
  //   window.scroll({top: 0, left: 0, behavior: 'smooth' });
  // });
  var answerBtn = document.getElementById("checkAnswerBtn");
  if (answerBtn) answerBtn.addEventListener('click', checkAnswer);
  var resetBtn = document.getElementById("resetBtn");
  if (resetBtn) resetBtn.addEventListener('click', resetCloze);
  // var vocBtn = document.getElementById("vocBtn");
  // if (vocBtn) vocBtn.addEventListener('click', toggleVoc);
  // var vocModeBtn = document.getElementById("vocModeBtn");
  // if (vocModeBtn) vocModeBtn.addEventListener('click', changeVocMode);
}

document.addEventListener("DOMContentLoaded", function() {
  var loc = window.location.pathname.replace(/\//g,'')
  if (loc != 'mcgill-korean') {
    eventListeners();
    replaceCloze();
  }
});

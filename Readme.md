Jekyll and bundle is required for this project

# To install

```
git clone https://gitlab.com/htyao/mcgill-korean.git
cd mcgill-korean
bundle install
```

Run it with
```
jekyll serve
```

# @Author: htyao
# @Date:   2020-05-16T22:22:23+02:00
# @Last modified by:   htyao
# @Last modified time: 2020-05-23T01:46:16+02:00



import sys
from datetime import date
from docx import Document
import re

op = re.compile("(.*)\((.*)")
cp = re.compile("(.*)\)(.*)")

def printParticlePage():
    """
    Write Particle practice file
    """
    with open('_posts/{}-{}-Particle-연습.html'.format(DATE, SLG), 'w') as f:
        print("---", file = f)
        print("title: Particle 연습".format(TITLE), file = f)
        print("layout: cloze_page", file = f)
        print("categories: {}".format(CAT), file = f)
        print("tags: cloze", file = f)
        print("---", file = f)
        for p in particleText:
            print("<p>{}</p>".format(p), file = f)

def printVerbPage():
    """
    Write Verb/Adj practice file
    """
    with open('_posts/{}-{}-Verb-Adjective-Conjugation-연습.html'.format(DATE, SLG), 'w') as f:
        print("---", file = f)
        print("title: Verb/Adjective Conjugation 연습", file = f)
        print("layout: cloze_page", file = f)
        print("categories: {}".format(CAT), file = f)
        print("tags: cloze", file = f)
        print("---", file = f)
        for p in verbText:
            print("<p>{}</p>".format(p), file = f)

def printVocPage():
    """
    Write Original file (with inner voc)
    """
    with open('_posts/{}-{}-vocabulary.html'.format(DATE, SLG), 'w') as f:
        print("---", file = f)
        print("title: {}".format(TITLE), file = f)
        print("layout: voc_page", file = f)
        print("categories: {}".format(CAT), file = f)
        print("tags: vocabulary", file = f)
        print("---", file = f)
        t = ""
        for p in vocText:
            t += "<p>{}</p>\n".format(p)
        print(t.format(*vocList), file = f)

if __name__ == "__main__":
    fileName = sys.argv[1]
    doc = Document(fileName)

    DATE = date.today().isoformat()
    TITLE = doc.paragraphs[0].text
    SLG = TITLE.replace(" ", "-")
    CAT = TITLE.replace(" ", "")
    originText = []
    vocText = []
    particleText = []
    verbText = []
    vocTmp = ""
    underNb = 0
    underList = []
    # Parse by paragraph
    for p in doc.paragraphs[1:]:
        voc = ""
        particle = ""
        verb = ""
        under = ""
        vHintList = []
        if "단어" in p.text:
            continue
        elif p.text:
            vHint = False
            v = ""
            for run in p.runs:
                # print("\'{}\'".format(run.text))
                # Check word's color
                # Red for particle
                # Blue for verb/adj
                # Purple for vocabulary section
                color = str(run.font.color.rgb)
                opm = op.match(run.text)
                cpm = cp.match(run.text)
                if color == 'FF0000':
                    particle += '<cloze data-hint="">{}</cloze>{}'.format(run.text[0], run.text[1:])
                    verb += run.text
                elif color in ['0070C0', '2E74B5']:
                    if opm:
                        v += opm.group(1)
                        particle += v
                        verb += '<cloze data-hint="{}">{}</cloze>'.format("{}", v)
                        vHint = True
                        v = opm.group(2)
                    elif cpm:
                        vHintList.append(v + cpm.group(1))
                        vHint = False
                        particle += cpm.group(2)
                        verb += cpm.group(2)
                        v = ""
                    elif vHint:
                        v += run.text
                    elif run.text:
                        v += run.text
                elif color == '7030A0':
                    vocTmp += run.text
                    continue

                else:
                    particle += run.text
                    verb += run.text
                # Next step, check if the word is underlined (vocabulary)
                # Ignore anything within brackets
                # Store temporarily in `under` as long as underline exists
                if opm:
                    run.text = opm.group(1)
                if cpm:
                    run.text = cpm.group(2)
                if not vHint:
                    # True as long as underline exists
                    if run.underline:
                        under += run.text
                    # Become true right after underline, reset under
                    elif under:
                        voc += "<voc data-voc='{}' data-trans='{}'>{}</voc>".format("{}", "{}", under)
                        voc += run.text
                        underList.append(under)
                        underNb += 1
                        under = ""
                    else:
                        voc += run.text
            if particle:
                particleText.append(particle)
            if verb:
                verbText.append(verb.format(*vHintList))
            if voc:
                vocText.append(voc)
    vocList = []
    for t in vocTmp.split(","):
        l = t.split(":")
        assert len(l) == 2, "Separator missing. {}".format(t)
        vocList += l
    assert underNb == len(vocList)/2, "Underlined: {}, Voc: {}\n{}".format(underNb, len(vocList)/2,underList)
    printParticlePage()
    printVerbPage()
    printVocPage()

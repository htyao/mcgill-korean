# @Author: htyao
# @Date:   2020-05-24T14:41:01+02:00
# @Last modified by:   htyao
# @Last modified time: 2020-05-24T15:30:41+02:00

VOC_FORMAT = /<voc data-voc='([^']*)' data-trans='([^']*)'>/

module Jekyll
  module Voclist
    def tovoclist(text)
      text.scan(VOC_FORMAT)
    end
  end
end

Liquid::Template.register_filter(Jekyll::Voclist)
